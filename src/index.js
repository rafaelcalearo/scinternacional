import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import * as firebase from 'firebase/app'
import 'firebase/firestore'

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyA_eINaRbM21UN8637ZRMmgycO6BSKJpnY",
  authDomain: "scinter.firebaseapp.com",
  databaseURL: "https://scinter.firebaseio.com",
  projectId: "scinter",
  storageBucket: "scinter.appspot.com",
  messagingSenderId: "692491614433",
  appId: "1:692491614433:web:426e046c20ce1a9833dfa0"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

ReactDOM.render(<App />, document.getElementById('root'));