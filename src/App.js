import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom"

import Header from './Header'
import Detalhes from './Detalhes'
import Jogadores from './Jogadores';

function App() {
  return (
    <Router>
      <Header/>
        <Route path="/" exact component={Jogadores} />
        <Route path="/detalhes/:id" component={Detalhes} />
    </Router>
  );
}

export default App;
