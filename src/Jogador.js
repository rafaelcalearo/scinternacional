import React from 'react'
import { Link } from 'react-router-dom'

const Jogador = props => (
  <div className="card">
    <img className="card-img-top" src={props.foto} alt='Tela' />
    <div className="card-body">
      <h4 className="card-title mb-0">{props.vulgo}</h4>
      <h6 className="card-subtitle mt-0 mb-2 text-muted">{props.completo}</h6>
      <p className="card-text"><strong>Nascimento:</strong> <span>{props.nascimento}</span><br />
        <strong>Posição:</strong> <em>{props.posicao}</em></p>
      <Link to={`/detalhes/${props.id}`} className="btn btn-danger btn-block">FICHA TÉCNICA</Link>
    </div>
  </div>
)

export default Jogador