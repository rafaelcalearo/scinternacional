import React, { Component } from 'react'

import * as firebase from 'firebase/app'
import 'firebase/firestore'

import Jogador from './Jogador'

export default class Jogadores extends Component {

  state = {
    jogadores: []
  }

  componentDidMount() {
    this.loadJogadores()
  }

  loadJogadores = () => {
    let jogadores = []

    firebase.firestore().collection('jogadores').orderBy('vulgo').onSnapshot(snapshot => {
      snapshot.docChanges().forEach(change => {
        if (change.type === "added") {
          jogadores.push({ id: change.doc.id, ...change.doc.data() })
        }
        if (change.type === "modified") {
          jogadores = jogadores.map(jogador => jogador.id === change.doc.id ?
            { id: change.doc.id, ...change.doc.data() }
            : jogador)
        }
        if (change.type === "removed") {
          jogadores = jogadores.filter(jogador => jogador.id !== change.doc.id)
        }
      })
      this.setState({ jogadores })
    })
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3 className="mt-4 mb-0">BEM-VINDO</h3>
            <h6 className="mt-0 mb-4 text-muted">PÁGINA DOS JOGADORES A SEREM VOTADOS POR NOTA</h6>
            <div className='card-columns'>
              {this.state.jogadores.map((jogador) => (
                <Jogador key={jogador.id}
                  id={jogador.id}
                  vulgo={jogador.vulgo}
                  completo={jogador.completo}
                  foto={jogador.foto}
                  nascimento={jogador.nascimento}
                  posicao={jogador.posicao} />
              ))}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
