import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Header extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-danger bg-danger">
        <div className="container">
        <Link to="/"><img src="https://logodownload.org/wp-content/uploads/2015/05/internacional-porto-alegre-logo-escudo.png" className="" alt="Logo" width="50" /></Link>
        <Link className="navbar-brand text-light" to="/">SPORT CLUB INTERNACIONAL</Link>
        <button className="navbar-toggler text-light" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Alterna navegação">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">            
            <li className="nav-item">
              <Link className="nav-link text-light" to="#">SEJA SÓCIO</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link text-light" to="#">INTER SHOP</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link text-light" to="#">CONTATO</Link>
            </li>
          </ul>
        </div>
        </div>
      </nav>
    )
  }
}
