import React from 'react'

const Notas = props => (
    props.ultimo === 1 ? <p style={{ backgroundColor: "#fbdfdf" }}><small>
        <strong>{props.nome}</strong> (<span className="text-muted">
            {props.email} — {props.fone}</span>), nota: <strong>{props.nota}</strong>.</small></p>
        : <p><small><strong>{props.nome}</strong> (<span className="text-muted">
            {props.email} — {props.fone}</span>), nota: <strong>{props.nota}</strong>.</small></p>
)

export default Notas